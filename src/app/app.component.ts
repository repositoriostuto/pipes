import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nombre = "Armando";
  nombre2  = "fernAndo alberto heRRera jimenez";

  arreglo = [1,2,3,4,5,6,7,8,9,10];

  PI = Math.PI;

  a = 0.234;

  salario = 1234.5;

  heroe = {
    nombre:"Elizabeth Halsen",
    clave:"Bruja Escarlata",
    edad:"32",
    direccion:{
      calle:"Vision House",
      casa:"22",
    }
  }

  valorDePromesa = new Promise( (resolve, reject) =>{
    setTimeout( ()=>resolve('Llego la data!'), 3500);
  } )

  fecha = new Date();

  video = "Enc80-YUuZc";

  activar:boolean = true;
}
